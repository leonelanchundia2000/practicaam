package com.example.practicasugarorm;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button ButtonGuardar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //crear
        ButtonGuardar = findViewById(R.id.ButtonGuardar);
        //crearunnuevolibro
        ButtonGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Book registro1 = new Book(
                        "titulo",
                        "Edicion"
                );
                registro1.save();
                Log.e("Guardar", "Datos guardados!");
            }
        });

    }
}
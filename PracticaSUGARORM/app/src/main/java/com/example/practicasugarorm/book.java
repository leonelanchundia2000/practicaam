package com.example.practicasugarorm;

import com.orm.SugarRecord;

public class Book extends SugarRecord<Book> {

    //Propiedades
    String title;
    String edition;

    //Constructor
    public Book() {
    }

    //Sobrecarga del constructor
    public Book(String title, String edition) {
        this.title = title;
        this.edition = edition;

    }


}

